# Hack The Box - Cap

```
Host Name: Cap
IP Address: 10.10.10.245
```
## Nmap Enumeration

Open Ports = 60, 21, 22

## Process of Penetration

- Tried to enumerate the directories by using gobuster and scannned the website using nikto.

- Browsing through the webpages I found a number of wevpages with packet data capture files in them which were available for download.

- I found the populated pcap files from these pages:

http://10.10.10.245/data/0 -- Contained FTP data.
http://10.10.10.245/data/7
http://10.10.10.245/data/8
http://10.10.10.245/data/9

- I found the user credentials for FTP from the first pcap file containing the FTP packet captures.

```
Username: nathan
Password: Buck3tH4TF0RM3!
```

- Logging into the FTP server we get the user flag

```
User-Flag: 7e2daf3f08032bc753fc1d8af2eeef0c
```

- Then I tried to connect to the SSH server with the same username and password.

- It allowed me to log in as nathan user.

## Privilege Escalation

- After running linpeas.sh and analyzing its output I found out this odd entry under capabilities:

```
Files with capabilities (limited to 50):
/usr/bin/python3.8 = cap_setuid,cap_net_bind_service+eip
```

- Now using gtfobins.github.io I found out that we can use this command to escalate our privileges:

```
./python3 -c 'import os; os.setuid(0); os.system("/bin/bash")'
```

- After running this command I got the root flag.

```
Root-Flag: 2244ff50d776992e3077cf0f8d2a849c
```